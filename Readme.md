# Bases JavaScript

# Partie 1: Variables

1. Créez une variable contenant votre citation préferée, et affichez la dans la console.  

2. Ecrivez un programme JavaScript pour trouver l'aire d'un triangle dont les longueurs sont égales à 5, 6, 7.  
Heron Formula: https://www.mathwarehouse.com/geometry/triangles/area/herons-formula-triangle-area.php  

# Partie 2: Conditions  

3. Ecrivez un programme JavaScript qui détermine si un entier N est impair ou pair.  

4. Ecrivez un programme JavaScript pour déterminer si une année donnée est une année bissextile dans le calendrier grégorien.  

5. Ecrivez un programme JavaScript qui prend un entier aléatoire compris entre 1 et 10, puis l’utilisateur est  invité à saisir un nombre approximatif. Si l'entrée utilisateur correspond au numéro approximatif, le programme affichera le message "Bon travail", sinon le message "Ne correspond pas".  

6. Ecrivez un programme JavaScript pour créer une nouvelle chaîne de caractères en ajoutant "You" devant la chaîne de caractères donnée. Si la chaîne donnée commence déjà par "You", retournez la chaîne de caractères d'origine comme elle est.
P.S: vous pouvez utiliser la méthode substring()

7. Ecrivez un programme JavaScipt qui test la valeur d'une note d'exam et affiche la mention très bien si la note est 20 , assez bien si la note est 17, passable si la note est 10 et inconnu si la note a d'autres valeurs.
En utilisant Switch.  

# Partie 3: Boucles 

8. Ecrire un programme JavaScript qui permet de calculer le produit de deux entiers mais en utilisant des additions successives.  

9. Ecrire un programme JavaScript permettant de lire un nombre entier N puis calcule son factoriel.  
Utilisez for,  
Utilisez while.  

10. Ecrivez un programme JavaScript pour compter le nombre de voyelles dans une chaîne de caractères donnée.  

# Partie 4: Fonctions

11. Créez une fonction appelée buildTriangle() qui prend comme paramètre ( la largeur maximale du triangle) et renvoi la représentation sous forme d'un triangle.    
Voir l'exemple de sortie ci-dessous.  
```
buildTriangle(6);
résultat affiché:
* 
* * 
* * * 
* * * * 
* * * * * 
* * * * * * 
```

12. Ecrivez une fonction anonyme en stockant la fonction dans une variable appelée "rire" et affiche le nombre de "ha" que vous passez en tant qu'argument.  
```
rire(4);  
résultat: hahahaha  
```

13. Ecrivez une expression de fonction non anonyme qui stock la fonction dans une variable appelée "crier" et retourne le nombre de "a!" que vous passez en tant qu'argument.   

```
console.log(crier(6));  
résultat: aaaaaa!  
```

14. Fonctions inline:  
la fonction emotion est définit comme suit:  
```
function emotions(msgString, functionName, degree) {
    console.log("I'am " + msgString + ", " + functionName(degree));
}
```
Appelez la fonction emotions() pour qu'il affiche le resultat ci-dessous, mais au lieu d'avoir une fonction rire déja déclaré et l'appeller à l'intérieur de la fonction émotion comme dans l'exemple ci-dessus, vous déclarez la fonction rire directement à l'intérieur de l'appel à la fonction emotions.
```
emotions("heureux", rire, 2);  
résultat: "Je suis heureux, haha!"  
```

# Partie 5: Tableaux

15. Considérons le tableau suivant.
var actorsInFilm = ["Tom Hanks", "Jim Parson", "Johnny Depp", "Anthony Hopkins", "Anne Hathaway", "Rami Malek"];

Créez une fonction appelée isGoodMovie qui prend le tableau actorsInFilm  et le nom de l'acteur entré par l'utilisateur comme arguments et renvoie true si le tableau contient l'acteur entré par l'utilisateur.

16. Ecrivez un programme JavaScript pour permuter le premier et le dernier élément d'un tableau d'entiers. Le tableau doit contenir au moins un element.

17. Ecrivez un programme JavaScript pour trier un tableau de chaine de caracteres dans l'ordre croissant.

18. Utilisez la méthode map() pour prendre le tableau prixHT présenté ci-dessous et créer un second tableau prixTotal qui affiche le prix avec 20% de tva ajouté.
```
var prixHT = [50.23, 19.12, 34.01, 100.11, 12.15, 9.90, 29.11, 12.99, 10.00, 99.22, 102.20, 100.10, 6.77, 2.22];
```
Afficher le nouveau tableau dans la console.

19. Utilisez une boucle for imbriquée pour balayer les elements du tableau ci-dessous et remplacer tous les nombres pairs par la chaîne "paire" et tous les autres nombres par la chaîne "impaire".

```
var numbers = [
    [243, 12, 23, 12, 45, 45, 78, 66, 223, 3],
    [34, 2, 1, 553, 23, 4, 66, 23, 4, 55],
    [67, 56, 45, 553, 44, 55, 5, 428, 452, 3],
    [12, 31, 55, 445, 79, 44, 674, 224, 4, 21],
    [4, 2, 3, 52, 13, 51, 44, 1, 67, 5],
    [5, 65, 4, 5, 5, 6, 5, 43, 23, 4424],
    [74, 532, 6, 7, 35, 17, 89, 43, 43, 66],
    [53, 6, 89, 10, 23, 52, 111, 44, 109, 80],
    [67, 6, 53, 537, 2, 168, 16, 2, 1, 8],
    [76, 7, 9, 6, 3, 73, 77, 100, 56, 100]
];
```

# Partie 6: Objets

20. Créez un objet door qui a comme propriété color (string), isOpen (booléan) et deux fonctions open et close qui changent l'état de isOpen.

21. Ecrivez un programme JavaScript pour afficher les propriétés d'un objet JavaScript. 

```
var student = { 
name : "myName", 
class : "myClass", 
cursus : "myCursus" };
résultat: name,class,cursus
```

22. Créez un tableaux qui contient 28 apprenants (chaque apprenant est un objet qui contient deux proprietés lastName et firstName  
Créez une fonction qui genere 5 groupes aléatoire de 5 à 6 apprenants à partir du tableau des 28 apprenants  
Créez Une fonction qui choisi une groupe aleatoire de ces 5 groupes générés  

# Partie 7: DOM

23. Soit une page HTML qui contient de text dans une paragraphe et un bouton. En cliquant sur le bouton, le font, le taille du font et la couleur de texte sont changé. 
```
<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8 />
<title>JS DOM paragraph style</title>
</head> 
<body>
<p id ='text'>JavaScript Exercises</p> 
<div>
<button id="jsstyle"
onclick="js_style()">Style</button>
</div>
</body>
</html>
``` 

24. Ecrivez une fonction JavaScript pour obtenir les valeurs écrites dans le champ de texte du formulaire.  
fichier HTML:  
```
<!DOCTYPE html>
<html><head>
<meta charset=utf-8 />
<title>Return first and last name from a form - w3resource</title>
</head><body>
<form id="form1" onsubmit="getFormvalue()">
First name: <input type="text" name="fname" value="David"><br>
Last name: <input type="text" name="lname" value="Beckham"><br>
<input type="submit" value="Submit">
</form>
</body>
</html>
```

25. soit le document html suivant :

```
<!doctype html>
<html>
<head>
	<title>styles</title>
	<meta charset="utf-8">
</head>
<body>
   <p id="parag1">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
   tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
   quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
   consequat.
   </p>
   <button onclick="changer_style()">Changer Style</button>

</body>

```
Ecrire la fonction changer_style qui permet de styler le paragraphe au clic du bouton par :
une couleur blanche.
un background noir.
une bordure noire de 1px.
Définir les propriétés précédentes dans une classe "active" et modifier la fonction changer_style de telle façon qu'elle ajoute la classe "active" au paragraphe.

